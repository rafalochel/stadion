package stadion.controller;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import stadion.persistence.HibernateUtil;

/**
 * Created by student on 03.05.17.
 */
public class DatabaseTestFixture {

    @BeforeClass
    public static void setUpClass() {
        HibernateUtil.initTestConfiguration();
    }


    @AfterClass
    public static void tearDownClass() {
        HibernateUtil.shutdown();
    }
}
