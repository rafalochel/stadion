package stadion.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import stadion.model.User;

import static org.junit.Assert.*;

/**
 * Created by student on 11.05.17.
 */
public class UserControllerReputationTest extends DatabaseTestFixture{

    UserController controller = new UserController();
    private String name = "Imie";
    private String surname = "Nazwisko";
    private String login = "Login";
    private String password = "Haselko";
    private String email = "email@email.com";


    @Test
    public void add_reputation_should_be_one() throws Exception{
        User user = controller.getUsersList().get(0);
        controller.incrementUserReputation(user.id);

        User userAfter = controller.getUsersList().get(0);
        Assert.assertEquals(1, userAfter.getReputation());
    }

    @Test
    public void remove_reputation_should_be_minus_one() throws Exception{
        User user = controller.getUsersList().get(0);
        controller.decrementUserReputation(user.id);

        User userAfter = controller.getUsersList().get(0);
        Assert.assertEquals(-1, userAfter.getReputation());

    }

    @Before
    public void setUp() throws Exception{
        controller.registerUser(name, surname, login, password.toCharArray(), email);
    }

    @After
    public void tearDown() throws Exception {
        controller.deleteUser(controller.getUsersList().get(0));
    }

}