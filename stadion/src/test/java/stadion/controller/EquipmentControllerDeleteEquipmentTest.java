package stadion.controller;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import stadion.model.Equipment;

/**
 * Created by student on 03.05.17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EquipmentControllerDeleteEquipmentTest extends DatabaseTestFixture {
    String placeName = "place";
    String equipmentName = "equipment";

    private void createPlace(String name, int capacity) throws Exception{
        PlaceController controller = new PlaceController();
        controller.addPlace(name, capacity);
    }

    private void createEquipment(String name, String placeName) throws Exception{
        EquipmentController controller = new EquipmentController();
        controller.addEquipment(name, placeName);
    }

    public void act(Equipment equipment) throws Exception{
        EquipmentController controller = new EquipmentController();
        controller.deleteEquipment(equipment);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void delete_equipment() throws Exception{
        createPlace(placeName, 1);
        createEquipment(equipmentName,placeName);
        EquipmentController controller = new EquipmentController();
        Equipment equipment = controller.getEquipmentFromName(equipmentName);
        Assert.assertNotNull(equipment);

        act(equipment);

        equipment = controller.getEquipmentFromName(equipmentName);
        Assert.assertNull(equipment);
    }
}