package stadion.controller;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.junit.*;
import org.junit.runners.MethodSorters;
import stadion.exception.InvalidFormatException;
import stadion.exception.NameAlreadyInUseException;
import stadion.model.Place;
import stadion.persistence.HibernateUtil;



/**
 * Created by wiewiogr on 18.04.17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PlaceControllerTest {
    private String name = "Miejsce";
    private int capacity = 10;

    @BeforeClass
    public static void setUpClass() {
        HibernateUtil.initTestConfiguration();
    }

    @Test
    public void test1_add_place_test_with_valid_data() throws Exception{
        PlaceController controller = new PlaceController();
        controller.addPlace(name, capacity);
        Assert.assertNotNull(controller.getPlaceFromName(name));
    }

    @Test(expected = NameAlreadyInUseException.class)
    public void test2_add_place_with_name_that_alread_exists_should_throw_exception() throws Exception{
        PlaceController controller = new PlaceController();
        controller.addPlace(name, capacity);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1_add_place_with_negative_capacity_should_throw_exception() throws Exception{
        PlaceController controller = new PlaceController();
        int negativeCapacity = -1;
        controller.addPlace(name, negativeCapacity);
    }

    @Test
    public void test1_add_place_with_name_in_wrong_format() throws Exception{
        PlaceController controller = new PlaceController();
        String[] listOfInvalidNames = {"Nm", "", "asdf&&$$",
                "TooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLong",};
        for(String invalidName : listOfInvalidNames) {
            try {
                controller.addPlace(invalidName, capacity);
                Assert.fail("Expected InvalidFormatException");
            } catch (InvalidFormatException ex) {
                Assert.assertTrue(ex.toString().contains("Name"));
            }
        }
    }

    @Test
    public void test2_get_all_places_length_should_be_one() throws Exception{
        PlaceController controller = new PlaceController();
        Assert.assertEquals(1,controller.getAllPlaces().size());
    }


    @Test
    public void test3_delete_place() throws Exception{
        PlaceController controller = new PlaceController();
        Place place = controller.getPlaceFromName(name);

        int numberOfPlaces = controller.getAllPlaces().size();
        controller.deletePlace(place);
        Assert.assertEquals(numberOfPlaces-1,controller.getAllPlaces().size());
    }


    @AfterClass
    public static void tearDownClass() {
        HibernateUtil.shutdown();
    }
}
