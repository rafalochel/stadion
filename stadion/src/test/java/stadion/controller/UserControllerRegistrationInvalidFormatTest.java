package stadion.controller;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import stadion.exception.InvalidFormatException;
import stadion.persistence.HibernateUtil;

/**
 * Created by wiewiogr on 13.04.17.
 */
public class UserControllerRegistrationInvalidFormatTest {
    private String name = "Imie";
    private String surname = "Nazwisko";
    private String login = "Login";
    private String password = "Haselko";
    private String email = "email@email.com";

    @BeforeClass
    public static void setUpClass() {
        HibernateUtil.initTestConfiguration();
    }

    @Test
    public void register_user_with_invalid_name_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String[] listOfInvalidNames = {"startingWithLowerCase", "Nm",
        "TooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLong",};
        for(String invalidName : listOfInvalidNames) {
            try {
                controller.registerUser(invalidName, surname, login, password.toCharArray(), email);
                Assert.fail("Expected InvalidFormatException");
            } catch (InvalidFormatException ex) {
                Assert.assertTrue(ex.toString().contains("Name"));
            }
        }
    }

    @Test
    public void register_user_with_invalid_surname_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String[] listOfInvalidSurnames = {"startingWithLowerCase", "Nm",
                "TooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLong",};
        for(String invalidSurname : listOfInvalidSurnames) {
            try {
                controller.registerUser(name, invalidSurname, login, password.toCharArray(), email);
                Assert.fail("Expected InvalidFormatException");
            } catch (InvalidFormatException ex) {
                Assert.assertTrue(ex.toString().contains("Surname"));
            }
        }
    }

    @Test
    public void register_user_with_invalid_username_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String[] listOfInvalidUsernames = {"Nm",
                "TooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLong",};
        for(String invalidUsername : listOfInvalidUsernames) {
            try {
                controller.registerUser(name, surname, invalidUsername, password.toCharArray(), email);
                Assert.fail("Expected InvalidFormatException");
            } catch (InvalidFormatException ex) {
                Assert.assertTrue(ex.toString().contains("Username"));
            }
        }
    }

    @Test
    public void register_user_with_invalid_email_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String invalid_email = "email";
        try{
            controller.registerUser(name, surname, login, password.toCharArray(), invalid_email);
            Assert.fail("Expected InvalidFormatException");
        } catch (InvalidFormatException ex){
            Assert.assertTrue(ex.toString().contains("Email"));
        }
    }

    @Test
    public void register_user_with_invalid_password_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String invalid_password = "pass"; // too short
        try{
            controller.registerUser(name, surname, login, invalid_password.toCharArray(), email);
            Assert.fail("Expected InvalidFormatException");
        } catch (InvalidFormatException ex){
            Assert.assertTrue(ex.toString().contains("Password"));
        }
    }

    @AfterClass
    public static void tearDownClass() {
        HibernateUtil.shutdown();
    }
}
