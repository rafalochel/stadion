package stadion.controller;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import stadion.exception.InvalidFormatException;
import stadion.exception.NameAlreadyInUseException;
import stadion.model.Place;

/**
 * Created by student on 03.05.17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PlaceControllerEditPlaceTest extends DatabaseTestFixture {
    String placeName = "place";

    private void createPlace(String name, int capacity) throws Exception{
        PlaceController controller = new PlaceController();
        controller.addPlace(name, capacity);
    }

    public void act(Place place, String newName, int newCapacity ) throws InvalidFormatException, NameAlreadyInUseException {
        PlaceController controller = new PlaceController();
        controller.updatePlace(place, newName, newCapacity);
    }


    @Test
    public void t0_update_place_with_valid_data() throws Exception {
        createPlace(placeName, 1);
        String newPlaceName = "neName";
        int newCapacity = 2;
        PlaceController controller = new PlaceController();
        Place place = controller.getPlaceFromName(placeName);

        act(place, newPlaceName, newCapacity);

        Place newPlace = controller.getPlaceFromName(newPlaceName);
        Assert.assertNotNull(newPlace);
        Assert.assertEquals(newCapacity, newPlace.getCapacity());
    }

    @Test
    public void t1_update_place_with_invalid_name_format_should_throw_exception() throws Exception{
        createPlace(placeName, 1);
        PlaceController controller = new PlaceController();
        Place place = controller.getPlaceFromName(placeName);
        String[] listOfInvalidNames = {"Nm", "", "asdf&&$$",
                "TooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLong",};
        for(String invalidName : listOfInvalidNames) {
            try {
                act(place,invalidName, 1);
                Assert.fail("Expected InvalidFormatException with string: " + invalidName);
            } catch (InvalidFormatException ex) {
                Assert.assertTrue(ex.toString().contains("Name"));
            }
        }
    }

    @Test(expected = NameAlreadyInUseException.class)
    public void t2_update_place_with_name_that_already_exists_should_throw_exception() throws Exception{
        PlaceController controller = new PlaceController();
        Place place = controller.getPlaceFromName(placeName);
        String newName = "occupiedName";
        createPlace(newName, 1);
        act(place,newName, 1);
    }
}