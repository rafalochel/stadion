package stadion.controller;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import stadion.model.Booking;
import stadion.model.Equipment;

import java.text.SimpleDateFormat;
import java.util.HashSet;

/**
 * Created by student on 11.05.17.
 */
public class BookingControllerSetRegistrationStatusTest extends DatabaseTestFixture{
    UserController userController = new UserController();
    BookingController bookingController = new BookingController();

    @Test
    public void set_collected_status_user_should_have_reputation_increased() throws Exception{

        if(bookingController.getAllBookingsFromPlace(0).size() == 0)
            Assert.fail();
        int id = bookingController.getAllBookingsFromPlace(0).get(0).getId();
        int currentReputation = userController.getUserFromId(MainController.loggedUser.getId()).getReputation();

        bookingController.setBookingCollected(id);

        Booking booking = bookingController.getAllBookingsFromPlace(0).get(0);
        Assert.assertEquals(1, booking.getStatus());
        int reputationAfter = userController.getUserFromId(MainController.loggedUser.getId()).getReputation();
        Assert.assertEquals(currentReputation+1, reputationAfter);

    }

    @Test
    public void set_uncollected_status_user_should_have_reputation_decreased() throws Exception{
        int id = bookingController.getAllBookingsFromPlace(0).get(0).getId();
        int currentReputation = userController.getUserFromId(MainController.loggedUser.getId()).getReputation();

        bookingController.setBookingUncollected(id);

        Booking booking = bookingController.getAllBookingsFromPlace(0).get(0);
        Assert.assertEquals(-1, booking.getStatus());
        int reputationAfter = userController.getUserFromId(MainController.loggedUser.getId()).getReputation();
        Assert.assertEquals(currentReputation-1, reputationAfter);

    }

    @After
    public void tearDown() throws Exception {
        if(userController.getUsersList().size() > 0) {
            userController.deleteUser(userController.getUsersList().get(0));
        }
    }

    @Before
    public void setUp() throws Exception{
        if(userController.getUsersList().size() == 0)
            userController.registerUser("Name", "Surname", "Login",
                    "Password".toCharArray(), "email@email.com");
        MainController.loggedUser = userController.getUsersList().get(0);
        PlaceController placeController = new PlaceController();
        if(placeController.getAllPlaces().size() == 0){
            placeController.addPlace("nazwa", 10);
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm");
        java.util.Date date = simpleDateFormat.parse("18.09.2016");
        java.util.Date time = simpleTimeFormat.parse("14:00");
        bookingController.addBooking(0,date,time, new HashSet<Equipment>());
    }

}