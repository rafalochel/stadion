package stadion.controller;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import stadion.exception.InvalidFormatException;
import stadion.exception.NameAlreadyInUseException;
import stadion.model.Equipment;

import java.util.Set;

/**
 * Created by student on 03.05.17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EquipmentControllerAddEquipmentTest extends DatabaseTestFixture {
    String placeName = "Place";
    int placeCapacity = 120;
    String equipmentName = "equipment";

    void act(String equipmentName, String placeName) throws InvalidFormatException, NameAlreadyInUseException {
        EquipmentController equipmentController = new EquipmentController();
        equipmentController.addEquipment(equipmentName, placeName);
    }

    @Test
    public void t0_add_equipment_to_place_it_should_have_equipment_attached() throws Exception{
        PlaceController controller = new PlaceController();
        controller.addPlace(placeName, placeCapacity);

        act(equipmentName, placeName);

        Set<Equipment> equipments = controller.getPlaceFromName(placeName).getEquipments();
        Assert.assertEquals(equipments.size(), 1);
    }

    @Test
    public void t0_add_equipment_with_illegal_name_should_throw_exception() throws Exception{
        String[] listOfInvalidNames = {"Nm", "", "asdf&&$$",
                "TooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLong",};
        for(String invalidName : listOfInvalidNames) {
            try {
                act(invalidName, placeName);
                Assert.fail("Expected InvalidFormatException with string: " + invalidName);
            } catch (InvalidFormatException ex) {
                Assert.assertTrue(ex.toString().contains("Name"));
            }
        }
    }

    @Test(expected = NameAlreadyInUseException.class)
    public void t1_add_equipment_with_name_that_already_exists_should_throw_exception() throws Exception{
        act(equipmentName, placeName);
    }
}