package stadion.controller;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import stadion.exception.InvalidFormatException;
import stadion.exception.NameAlreadyInUseException;
import stadion.model.Equipment;
import stadion.model.Place;

/**
 * Created by student on 03.05.17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EquipmentControllerEditEquipmentTest extends DatabaseTestFixture {

    String placeName1 = "place1";
    String placeName2 = "place2";
    String equipmentName = "equipment";

    public void act(Equipment equipment, String newName, String newPlaceName) throws Exception{
        EquipmentController controller = new EquipmentController();
        controller.updateEquipment(equipment, newName, newPlaceName);
    }

    private void createPlace(String name, int capacity) throws Exception{
        PlaceController controller = new PlaceController();
        controller.addPlace(name, capacity);
    }

    private void createEquipment(String name, String placeName) throws Exception{
        EquipmentController controller = new EquipmentController();
        controller.addEquipment(name, placeName);
    }

    @Test
    public void t0_update_equipment_with_valid_data() throws Exception{
        createPlace(placeName1, 1);
        createPlace(placeName2, 1);
        createEquipment(equipmentName,placeName1);
        String newName = "newName";
        EquipmentController controller = new EquipmentController();
        Equipment equipment = controller.getEquipmentFromName(equipmentName);

        act(equipment,newName, placeName2);

        PlaceController placeController = new PlaceController();
        Equipment newEquipment = controller.getEquipmentFromName(newName);
        Assert.assertNotNull(newEquipment);
        Place place = placeController.getPlaceFromName(placeName2);
        Assert.assertNotNull(place.getEquipments().contains(newEquipment));
    }

    @Test
    public void t1_update_equipment_with_invalid_name_format_should_throw_exception() throws Exception{
        createEquipment(equipmentName,placeName1);
        EquipmentController controller = new EquipmentController();
        Equipment equipment = controller.getEquipmentFromName(equipmentName);
        String[] listOfInvalidNames = {"Nm", "", "asdf&&$$",
                "TooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLongTooLong",};
        for(String invalidName : listOfInvalidNames) {
            try {
                act(equipment,invalidName, placeName1);
                Assert.fail("Expected InvalidFormatException with string: " + invalidName);
            } catch (InvalidFormatException ex) {
                Assert.assertTrue(ex.toString().contains("Name"));
            }
        }
    }

    @Test(expected = NameAlreadyInUseException.class)
    public void t2_update_equipment_with_name_that_already_exists_should_throw_exception() throws Exception{
        EquipmentController controller = new EquipmentController();
        Equipment equipment = controller.getEquipmentFromName(equipmentName);
        String newName = "occupiedName";
        createEquipment(newName,placeName1);
        act(equipment,newName, placeName1);
    }
}