package stadion.controller;

import org.junit.*;
import org.junit.runners.MethodSorters;
import stadion.exception.InvalidFormatException;
import stadion.exception.UsernameOrEmailAlreadyInUseException;
import stadion.model.User;
import stadion.persistence.HibernateUtil;

import java.util.List;

/**
 * Created by grzegorz on 25.04.17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerUpdateUserTest {
    private String name = "Imie";
    private String surname = "Nazwisko";
    private String login = "Login";
    private String password = "Haselko";
    private String email = "email@email.com";

    @BeforeClass
    public static void setUpClass() {
        HibernateUtil.initTestConfiguration();
    }

    @Test
    public void test0_register_and_update_user_with_valid_data() throws Exception{
        UserController controller = new UserController();
        try{
            controller.registerUser(name, surname, login, password.toCharArray(), email);
        } catch(Exception ex) {
            ex.printStackTrace();
            Assert.fail();
        }
        String newNick = "Nowynick";
        User user = controller.getUsersList().get(0);
        controller.updateUser(user,newNick,name,surname,email,3);
        Assert.assertTrue(user.getUsername() == newNick);
    }

     @Test(expected = UsernameOrEmailAlreadyInUseException.class)
     public void test1_update_user_with_used_email_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String usedEmail = "used@mail.com";
        User user = controller.getUsersList().get(0);
        controller.registerUser(name,surname,"Unusedusername","PASSWORD".toCharArray(),usedEmail);
        controller.updateUser(user,login,name,surname,usedEmail,3);
    }

    @Test(expected = InvalidFormatException.class)
    public void test1_update_user_with_invalid_data_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        User user = controller.getUsersList().get(0);
        controller.updateUser(user,login,name,surname,"WRONGEMAIL",3);
    }



    @AfterClass
    public static void tearDownClass() {
        HibernateUtil.shutdown();
    }

}