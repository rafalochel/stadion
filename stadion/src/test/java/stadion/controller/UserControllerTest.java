package stadion.controller;

import org.junit.*;
import org.junit.runners.MethodSorters;
import stadion.exception.*;
import stadion.model.User;
import stadion.persistence.HibernateUtil;

import java.util.List;

/**
 * Created by wiewiogr on 04.04.17.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest {
    private String name = "Imie";
    private String surname = "Nazwisko";
    private String login = "Login";
    private String password = "Haselko";
    private String email = "email@email.com";

    @BeforeClass
    public static void setUpClass() {
        HibernateUtil.initTestConfiguration();
    }

    @Test
    public void test0_list_of_users_before_register_should_be_empty() {
        UserController controller = new UserController();
        List<User> list = controller.getUsersList();
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void test1_register_user_with_valid_data() throws Exception {
        UserController controller = new UserController();
        try{
            controller.registerUser(name, surname, login, password.toCharArray(), email);
        } catch(Exception ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }

    @Test(expected = UserNotActivatedException.class)
    public void test2_login_inactive_user_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        User user = controller.loginUser(login,password.toCharArray());
    }

    @Test
    public void test2_list_of_users_after_register_should_not_be_empty() {
        UserController controller = new UserController();
        List<User> list = controller.getUsersList();
        Assert.assertFalse(list.isEmpty());
    }

    @Test(expected = InvalidCredentialException.class)
    public void test2_login_with_invalid_credentials_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String wrongPassword = "zle haslo";
        User user = controller.loginUser(login,wrongPassword.toCharArray());
    }

    @Test(expected = UserDoesNotExistsException.class)
    public void test2_login_with_user_credentials_that_does_not_exists_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String wrongLogin = "JakisLogin";
        User user = controller.loginUser(wrongLogin,password.toCharArray());
    }

    @Test(expected = UsernameOrEmailAlreadyInUseException.class)
    public void test2_register_user_with_already_taken_username_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String otherEmail = "mail@mail.pl";
        controller.registerUser(name, surname, login, password.toCharArray(), otherEmail);
    }

    @Test(expected = UsernameOrEmailAlreadyInUseException.class)
    public void test2_register_user_with_already_taken_email_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String otherLogin = "User123";
        controller.registerUser(name, surname, otherLogin, password.toCharArray(), email);
    }

    @Test(expected = UserDoesNotExistsException.class)
    public void test2_activate_user_that_does_not_exists_should_throw_exception() throws Exception {
        UserController controller = new UserController();
        String wrongLogin = "BlednyLoginNieWystepujacyWBazie";
        boolean state = controller.activateUser(wrongLogin);
    }

    @Test
    public void test3_activate_user_who_is_registered() {
        UserController controller = new UserController();
        try {
            controller.activateUser(login);
        } catch(Exception ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }
    @Test
    public void test4_login_to_activated_user_account() {
        UserController controller = new UserController();
        try {
            controller.loginUser(login,password.toCharArray());
        } catch(Exception ex) {
            ex.printStackTrace();
            Assert.fail();
        }
    }
    @Test
    public void test5_get_user_from_id() throws Exception {
        UserController controller = new UserController();
        List<User> users = controller.getUsersList();
        User user2 = controller.getUserFromId(users.get(0).getId());
        Assert.assertTrue(users.get(0).getId() == user2.getId());


    }
    @Test(expected = UserDoesNotExistsException.class)
    public void test5_getting_user_with_not_existing_id_should_throw_exception()  throws Exception{
        UserController controller = new UserController();
        controller.getUserFromId(-999);
    }

    @Test
    public void test6_delete_user_should_change_users_count() {
        UserController controller = new UserController();
        List<User> users = controller.getUsersList();
        controller.deleteUser(users.get(0));
        List<User> newUsers = controller.getUsersList();
        Assert.assertTrue(users.size() - 1 == newUsers.size());
    }

    @AfterClass
    public static void tearDownClass() {
        HibernateUtil.shutdown();
    }
}