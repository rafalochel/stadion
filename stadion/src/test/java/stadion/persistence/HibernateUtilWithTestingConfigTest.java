package stadion.persistence;

import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wiewiogr on 09.04.17.
 */
public class HibernateUtilWithTestingConfigTest {
    @Test
    public void test_init_test_and_get_open_session_from_session_factory() throws Exception {
        try {
            HibernateUtil.initTestConfiguration();
            Session session = HibernateUtil.getSessionFactory().openSession();
        } catch(Exception ex){
            Assert.fail();
        }
    }
}
