package stadion.persistence;

import org.hibernate.Session;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by wiewiogr on 04.04.17.
 */
public class HibernateUtilTest {

    @BeforeClass
    public static void setUpClass(){
    }

    @Test
    public void test_get_open_session_from_session_factory() throws Exception {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
        } catch(Exception ex){
            ex.printStackTrace();
            Assert.fail();
        }
    }
}