package stadion.controller;

import org.hibernate.Session;
import org.hibernate.query.Query;
import stadion.exception.InvalidFormatException;
import stadion.exception.NameAlreadyInUseException;
import stadion.model.Equipment;
import stadion.model.Place;
import stadion.persistence.HibernateUtil;

import java.util.List;
import java.util.Set;

/**
 * Created by rafal on 07.04.17.
 */
public class PlaceController {

    public void addPlace(String name, int capacity) throws IllegalArgumentException, InvalidFormatException, NameAlreadyInUseException {
        if(capacity <= 0) throw new IllegalArgumentException("Capacity is negative or zero.");

        if(!name.matches("^[A-Za-z0-9 ]{3,32}$"))
            throw new InvalidFormatException("Name in not valid.");

        if(isPlaceNameAlreadyInUse(name))
            throw new NameAlreadyInUseException();

        Place place = new Place(name, capacity);
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(place);
        session.getTransaction().commit();
    }

    public List<Place> getPlacesThatMatchesString(String partOfName) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query q;
        if( partOfName.equals("")) {
            q = session.createQuery("from Place");
        }
        else {
            q = session.createQuery("from Place where name like :prt");
            q.setParameter("prt", "%"+partOfName+"%");
        }
        List<Place> list = q.list();
        session.getTransaction().commit();
        return list;
    }

    public int getIDFromName(String name) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query q = session.createQuery("from Place where name like :name");
        q.setParameter("name", name);
        List<Place> list = q.list();
        session.getTransaction().commit();
        if(list.size()<=0) {
            return -1;
        }
        int temp = list.get(0).getId();
        return temp;
    }

    public List<Place> getAllPlaces(){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Place");
        List<Place> all = query.list();
        session.getTransaction().commit();
        return all;
    }

    public Set<Equipment> getPlaceEquipment(int id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM Place WHERE id=:id");
        query.setParameter("id",id);
        List<Place> all = query.list();
        session.getTransaction().commit();
        Set<Equipment> eq = all.get(0).getEquipments();

        return eq;
    }

    private boolean isPlaceNameAlreadyInUse(String name){
        try{
            getPlaceFromName(name);
            return true;
        } catch(IndexOutOfBoundsException ex){

            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            session.getTransaction().rollback();
            return false;
        }
    }

    public Place getPlaceFromName(String name) throws IndexOutOfBoundsException{
        Place temp = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query=session.createQuery("from Place where name = '" + name + "'");
        temp = (Place) query.list().get(0);
        session.getTransaction().commit();
        return temp;
    }

    public String getPlaceName(int id) {
        Place temp = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query;
        query = session.createQuery("from Place where id = " + id);
        temp = (Place) query.list().get(0);
        session.getTransaction().commit();
        return temp.getName();
    }

    public void updatePlace(Place place, String newName, int newCapacity) throws NameAlreadyInUseException, InvalidFormatException {

        if(newCapacity <= 0) throw new IllegalArgumentException("Capacity is negative or zero.");

        if(!place.getName().equals(newName)) {
            if (!newName.matches("^[A-Za-z0-9 ]{3,32}$"))
                throw new InvalidFormatException("Name in not valid.");

            if (isPlaceNameAlreadyInUse(newName))
                throw new NameAlreadyInUseException();
            place.setName(newName);
        }
        place.setCapacity(newCapacity);

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(place);
        session.getTransaction().commit();

    }

    public void deletePlace(Place place){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(place);
        session.getTransaction().commit();
    }
}
