package stadion.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.query.Query;

import stadion.model.Booking;
import stadion.model.Equipment;
import stadion.model.Place;
import stadion.model.User;
import stadion.persistence.HibernateUtil;
import stadion.view.ArchiveView;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;


/**
 * Created by rafal on 26.04.17.
 */
public class BookingController {
    public void addBooking(int place, Date date, Date time, Set<Equipment> equip) throws ConstraintViolationException {
        Booking booking = new Booking();
        booking.setPlace(place);
        booking.setBeginDate(date);
        booking.setBeginTime(time);
        Set<Equipment> e = new HashSet<Equipment>();
        for (Equipment x:equip) {
            e.add(x);
        }
        booking.setEquipments(e);
        booking.setUserID(MainController.loggedUser.getId());
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.save(booking);
        session.getTransaction().commit();
    }
    public boolean isReserved(int place, String date, String time, List<Booking> bookings)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm");
        for (Booking b:bookings) {
            if (simpleDateFormat.format(b.getBeginDate()).equals(date) && simpleTimeFormat.format(b.getBeginTime()).equals(time)) {
                return true;
            }
        }
        return false;
    }

    public List<Booking> getAllBookingsFromPlace(int place) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Booking WHERE place = :id");
        q.setParameter("id",place);
        List<Booking> list = q.list();
        session.getTransaction().commit();
        return list;
    }

    public List<Booking> getCurrentUserBookings() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Booking WHERE userID = :id and (beginDate>:date or (beginDate=:date and beginTime>:time))");
        q.setParameter("id",MainController.loggedUser.getId());
        q.setParameter("date",new Date());
        q.setParameter("time",new Date());
        List<Booking> list = q.list();
        session.getTransaction().commit();
        return list;
    }

    public List<Booking> getUserBookings(User user) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Booking WHERE userID = :id");
        q.setParameter("id",user.getId());
        List<Booking> list = q.list();
        session.getTransaction().commit();
        return list;
    }


    public Booking getIncomingBookingById(int id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Booking WHERE id = :id and beginDate>:date or (beginDate=:date and beginTime>:time)");
        q.setParameter("id",id);
        q.setParameter("date",new Date());
        q.setParameter("time",new Date());
        List<Booking> list = q.list();
        session.getTransaction().commit();
        return list.get(0);
    }

    public void cancelBookingByID(int id) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query q = session.createQuery("DELETE FROM Booking WHERE id = :id");
        q.setParameter("id",id);
        q.executeUpdate();
        session.getTransaction().commit();
    }

    public List<Object[]> getBookingsInTime(Date begin, Date end) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Booking b, User u, Place p WHERE b.beginDate >= :qBegin AND b.beginDate <= :qEnd AND  u.id = b.userID AND p.id=b.place");
        q.setParameter("qBegin",begin);
        q.setParameter("qEnd",end);
        List bookings = q.list();
        session.getTransaction().commit();
        return bookings;
    }

    public void setBookingCollected(int id){
        Booking booking = getBookingFromId(id);
        UserController userController = new UserController();
        userController.incrementUserReputation(booking.getUserID());
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        booking.setStatus(1);
        session.update(booking);
        session.getTransaction().commit();
    }


    public void setBookingUncollected(int id){
        Booking booking = getBookingFromId(id);
        UserController userController = new UserController();
        userController.decrementUserReputation(booking.getUserID());
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        booking.setStatus(-1);
        session.update(booking);
        session.getTransaction().commit();
    }

    private Booking getBookingFromId(int id){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query q = session.createQuery("FROM Booking WHERE id = :id");
        q.setParameter("id",id);
        List<Booking> list = q.list();
        session.getTransaction().commit();
        return list.get(0);
    }
    public void generateReport(Date begin, Date end, File path, int type) {
        List<Object[]> bookings = getBookingsInTime(begin,end);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        if(type==1) {
            try {
                PrintWriter writer = new PrintWriter(path, "UTF-8");
                writer.println("StadiON report");
                writer.println(dateFormat.format(begin) + " - " + dateFormat.format(end));
                writer.println("");
                for (Object[] result : bookings) {
                    Booking booking = (Booking) result[0];
                    User user = (User) result[1];
                    Place place = (Place) result[2];
                    String line = booking.getId() + "\t" + user.getName() + " " + user.getSurname() + " (" + user.getUsername() + ")\t" + booking.getBeginDate() + " " + booking.getBeginTime() + "\t" + place.getName() + "\t";
                    for (Equipment equipment : booking.getEquipments())
                        line += equipment.getName() + " ";

                    writer.println(line);
                }
                writer.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Document document = new Document();
            try
            {
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(path));
                document.open();
                String title = "StadiON report for "  + dateFormat.format(begin) + " - " + dateFormat.format(end) + "\n";
                Paragraph paragraph = new Paragraph();
                paragraph.add(title);
                document.add(paragraph);


                document.add(new Paragraph("\n"));
                JTable bookingsTable = new JTable();
                bookingsTable.clearSelection();

                ArchiveController.fillTable(bookingsTable,begin,end,false);

                PdfPTable pdfTable = new PdfPTable(bookingsTable.getColumnCount());
                //adding table headers
                for (int j = 0; j < bookingsTable.getColumnCount(); j++) {
                    pdfTable.addCell(bookingsTable.getColumnName(j));
                }
                //extracting data from the JTable and inserting it to PdfPTable
                for (int rows = 0; rows < bookingsTable.getRowCount() - 1; rows++) {
                    for (int cols = 0; cols < bookingsTable.getColumnCount(); cols++) {
                        pdfTable.addCell(bookingsTable.getModel().getValueAt(rows, cols).toString());

                    }
                }
                pdfTable.setWidthPercentage(100);
                pdfTable.setWidths(new float[] { 1, 3,3,3,3,3 });
                document.add(pdfTable);


                document.close();
                writer.close();
                Desktop.getDesktop().open(path);
            } catch (DocumentException e)
            {
                e.printStackTrace();
            } catch (FileNotFoundException e)
            {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
