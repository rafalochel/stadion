package stadion.controller;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import stadion.DisplayImage;
import stadion.model.Equipment;
import stadion.model.Place;
import stadion.model.User;
import stadion.persistence.HibernateUtil;
import stadion.view.LoggedView;
import stadion.view.LoginView;
import stadion.view.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by wiewiogr on 04.04.17.
 */
public class MainController {
    JFrame loginFrame;
    JFrame loggedFrame;
    private LoggedView tabsPane;

    public static User loggedUser;
    private static MainController INSTANCE;

    public static MainController getInstance(){
        if(INSTANCE==null)
            INSTANCE = new MainController();
        return INSTANCE;
    }

    private JFrame createFrame(String name, View view){
        JFrame frame = new JFrame(name);
        frame.setContentPane(view.getMainPanel());
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        return frame;
    }

    public LoggedView getTabsPane() { return tabsPane; }

    private void showLoginFrame(){
        loginFrame.setVisible(true);
    }

    public void toggleLoginFrame() {
        if(loginFrame.isVisible())
        {
            loginFrame.setVisible(false);
        }
        else
        {
            loginFrame.setVisible(true);
        }
    }

    public void toggleLoggedFrame() {
        if(loggedFrame.isVisible())
        {
            loggedFrame.setVisible(false);
        }
        else
        {
            String title;
            title="Stadion - Logged as " + loggedUser.getName() + " " + loggedUser.getSurname() + " (" + loggedUser.getUsername() +")";
            loggedFrame.setTitle(title);
            loggedFrame.setVisible(true);
        }
    }

    public MainController(){

        DisplayImage wait = new DisplayImage("Stadion");
        ExecutorService exec = Executors.newSingleThreadExecutor();
        exec.execute(wait);
        try {
            HibernateUtil.getSessionFactory().getCurrentSession();
            loggedUser = new User();
            tabsPane = new LoggedView();
            loginFrame = createFrame("Stadion - Login/Register", new LoginView());
            loggedFrame = createFrame("", tabsPane);
            loggedFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            loggedFrame.addWindowListener(new WindowAdapter(){
                public void windowClosing(WindowEvent e){
                    int i=JOptionPane.showConfirmDialog(null, "Are you sure you want to logout?","Stadion - Logout",JOptionPane.YES_NO_OPTION);
                    if(i==0)
                    {
                        toggleLoggedFrame();
                        loggedUser = new User();
                        toggleLoginFrame();
                        Window[] w = Window.getWindows();
                        for (Window x:w
                                ) {
                            if(!x.equals(loginFrame)) {
                                x.setVisible(false);
                            }
                        }
                    }
                }
            });
        } catch(ExceptionInInitializerError ex) {
            JOptionPane.showMessageDialog(null, "Cannot connect to the server.", "Stadion", JOptionPane.ERROR_MESSAGE);
            Runtime.getRuntime().exit(0);
        }finally {
            wait.destroy();
            exec.shutdownNow();
        }

        wait = null;
        toggleLoginFrame();
    }
}
