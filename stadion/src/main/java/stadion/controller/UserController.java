package stadion.controller;

import org.hibernate.Session;
import org.hibernate.query.Query;
import stadion.exception.*;
import stadion.model.Booking;
import stadion.model.User;
import stadion.persistence.HibernateUtil;

import java.util.List;
import java.util.ListIterator;
import java.util.Set;

/**
 * Created by wiewiogr on 04.04.17.
 */
public class UserController {
    private String getHashfromString(String string) {
        String a = string;
        String b = a.toUpperCase();
        String c = a.concat(a);
        String d = c.concat(a);
        Integer a_hash = a.hashCode();
        Integer b_hash = b.hashCode();
        Integer c_hash = c.hashCode();
        Integer d_hash = d.hashCode();
        String hash = a_hash.toString() + b_hash.toString() + c_hash.toString() + d_hash.toString();
        return hash;
    }

    public void registerUser(String name, String surname, String username, char[] password, String email)
            throws InvalidFormatException, UsernameOrEmailAlreadyInUseException {
        String pass = new String(password);
        String errors = validateUserCredentials(name,surname,username,email);
        if (pass.length() < 6)
            errors += "Password must be at least 6 characters\n";

        if (errors.length() > 1)
            throw new InvalidFormatException(errors);

        if (!isUsernameAndEmailAvailable(username, email))
            throw new UsernameOrEmailAlreadyInUseException();
        String encryptedPassword = getHashfromString(pass);
        User user = new User(name, surname, username, email, encryptedPassword, -1);
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
    }

    public User loginUser(String name, char[] password) throws UserNotActivatedException, UserDoesNotExistsException, InvalidCredentialException {
        String pass = new String(password);
        String passwordHash = getHashfromString(pass);
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("from User where username = :name ");
        query.setParameter("name", name);
        List<User> list = query.list();
        session.getTransaction().commit();
        if (list.size() < 1) throw new UserDoesNotExistsException();
        if (list.get(0).getPassword().toString().equals(passwordHash)) {
            if (list.get(0).isActivated()) {
                User logged = new User(list.get(0).getName(), list.get(0).getSurname(), list.get(0).getUsername(), list.get(0).getEmail(), list.get(0).getPassword(), list.get(0).getRole());
                    return logged;
                } else {
                    throw new UserNotActivatedException();
                }
        } else {
            throw new InvalidCredentialException();
        }
    }

    public boolean activateUser(String name) throws UserDoesNotExistsException {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("from User where username = :name ");
        query.setParameter("name", name);
        List<User> list = query.list();
        if (list.size() < 1)
        {
            session.getTransaction().commit();
            throw new UserDoesNotExistsException();
        }
        else {
            list.get(0).setisActivated(true);
            list.get(0).setRole(3);
            session.save(list.get(0));
            session.getTransaction().commit();
        }
        return true;
    }

    private boolean isUsernameAndEmailAvailable(String username, String email) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM User WHERE username = :name OR email = :mail");
        query.setParameter("name", username);
        query.setParameter("mail", email);
        boolean isit = (query.list().size() == 0);
        session.getTransaction().commit();
        return isit;
    }

    public List<User> getUsersList() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM User");
        List<User> list = query.list();
        session.getTransaction().commit();
        return list;
    }

    public User getUserFromId(int id) throws UserDoesNotExistsException{
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM User WHERE id = :id");
        query.setParameter("id", id);
        List<User> lista = query.list();
        session.getTransaction().commit();
        if (lista.size() == 0)
            throw new UserDoesNotExistsException();
        User user = lista.get(0);
        return user;
    }

    public void updateUser(User user, String username, String firstName, String lastName, String email, int role) throws  UsernameOrEmailAlreadyInUseException, InvalidFormatException {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM User WHERE (username = :name OR email = :mail) AND id != :id");
        query.setParameter("name", username);
        query.setParameter("mail", email);
        query.setParameter("id", user.getId());
        List<User> lista = query.list();
        session.getTransaction().commit();

        if (lista.size() > 0) {
            throw new UsernameOrEmailAlreadyInUseException();
        }

        String errors = validateUserCredentials(firstName, lastName, username, email);

        if (errors.length() > 1)
            throw new InvalidFormatException(errors);
        user.setRole(role);
        user.setUsername(username);
        user.setName(firstName);
        user.setSurname(lastName);
        user.setEmail(email);
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(user);
        session.getTransaction().commit();
    }
    public void deleteUser(User user) {
        BookingController controller = new BookingController();
        for(Booking booking : controller.getUserBookings(user)){
            controller.cancelBookingByID(booking.getId());
        }
        user.setBookings(null);
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(user);
        session.getTransaction().commit();
    }
    private String validateUserCredentials(String name, String surname, String username, String email) {
        String errors = "";
        if(!name.matches("^[A-Z][A-Za-z]{2,31}$"))
            errors += "Name field in not valid\n";

        if(!surname.matches("^[A-Z][A-Za-z]{2,31}$"))
            errors += "Surname field in not valid\n";

        if(!username.matches("^[A-Za-z0-9]{3,32}$"))
            errors += "Username field in not valid\n";

        if(!email.matches("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"))
            errors += "Email field is not valid\n";


        return errors;
    }

    public User getUser(String name) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("FROM User where name is :name");
        query.setParameter("name",name);
        List<User> list = query.list();
        session.getTransaction().commit();
        return list.get(0);
    }


    public void incrementUserReputation(int id){
        changeReputation(id, 1);
    }

    public void decrementUserReputation(int id){
        changeReputation(id, -1);
    }

    private void changeReputation(int id, int value){
        User user = null;
        try {
            user = getUserFromId(id);
        } catch (UserDoesNotExistsException e) {
            e.printStackTrace();
        }

        int reputation = user.getReputation();
        user.setReputation(reputation+value);
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(user);
        session.getTransaction().commit();
    }

}
