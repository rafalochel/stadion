package stadion.controller;

import stadion.model.Booking;
import stadion.model.Equipment;
import stadion.model.Place;
import stadion.model.User;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by grzegorz on 16.05.17.
 */
public class ArchiveController {

    public static void updateRowHeights(JTable table)
    {
        for (int row = 0; row < table.getRowCount(); row++)
        {
            int rowHeight = table.getRowHeight();

            for (int column = 0; column < table.getColumnCount(); column++)
            {
                Component comp = table.prepareRenderer(table.getCellRenderer(row, column), row, column);
                rowHeight = Math.max(rowHeight, comp.getPreferredSize().height);
            }

            table.setRowHeight(row, rowHeight);
        }

    }
    public static void resizeColumnWidth(JTable table) {
        final TableColumnModel columnModel = table.getColumnModel();
        for (int column = 0; column < table.getColumnCount(); column++) {
            int width = 15; // Min width
            for (int row = 0; row < table.getRowCount(); row++) {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component comp = table.prepareRenderer(renderer, row, column);
                width = Math.max(comp.getPreferredSize().width +1 , width);
            }
            if(width > 300)
                width=300;
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }
    public static List<Object[]> fillTable(JTable bookingsTable, Date begin, Date end, boolean html) {
        bookingsTable.clearSelection();
        String[] columns ={ "ID","Place","Date","Customer", "Equipment", "Status" };
        BookingController bookingController = new BookingController();
        java.util.List<Object[]> bookingList;
        bookingList = bookingController.getBookingsInTime(begin,end);
        Object[][] rowData = new Object[bookingList.size()][columns.length];
        int i = 0;
        for (Object[] result : bookingList) {
            Booking booking = (Booking) result[0];
            User user = (User) result[1];
            Place place = (Place) result[2];
            int j = 0;

            rowData[i][j++] = booking.getId();
            rowData[i][j++] = place.getName();
            String eq = "";
            if(html) {
                rowData[i][j++] = "<html>" + booking.getBeginDate() + "<br>" + booking.getBeginTime() + "</html>";
                rowData[i][j++] = "<html>" + user.getId() + ": " + user.getUsername() + "<br>" + user.getName() + " " + user.getSurname() + "</html>";
                 eq += "<html>";
                for (Equipment equipment : booking.getEquipments())
                    eq += equipment.getName() + "<br>";
                eq += "</html>";
            } else {
                rowData[i][j++] = booking.getBeginDate() + " " + booking.getBeginTime();
                rowData[i][j++] = user.getId() + ": " + user.getUsername() +  "\n" + user.getName() + " " + user.getSurname();
                for (Equipment equipment : booking.getEquipments())
                    eq += equipment.getName() + "\n";
            }
            rowData[i][j++] = eq;
            rowData[i][j++] = booking.getStatusAsString();


            i++;
        }
        bookingsTable.setModel(new DefaultTableModel(rowData, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });
        bookingsTable.getColumnModel().getColumn(0).setPreferredWidth(20);
        ArchiveController.updateRowHeights(bookingsTable);
        ArchiveController.resizeColumnWidth(bookingsTable);

        return bookingList;
    }
}
