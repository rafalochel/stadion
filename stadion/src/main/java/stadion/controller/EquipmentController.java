package stadion.controller;

import org.hibernate.Session;
import stadion.exception.InvalidFormatException;
import stadion.exception.NameAlreadyInUseException;
import stadion.model.Equipment;
import stadion.model.Place;
import stadion.persistence.HibernateUtil;
import org.hibernate.query.Query;

import java.util.List;

/**
 * Created by student on 03.05.17.
 */
public class EquipmentController {
    public void addEquipment(String equipmentName, String placeName) throws InvalidFormatException, NameAlreadyInUseException {
        validateEquipmentsName(equipmentName);

        PlaceController placeController = new PlaceController();
        Place place = placeController.getPlaceFromName(placeName);
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Equipment equipment = new Equipment();
        equipment.setName(equipmentName);
        place.getEquipments().add(equipment);
        equipment.setPlace(place);
        session.saveOrUpdate(place);
        session.getTransaction().commit();
    }

    private boolean isEquipmentNameAlreadyInUse(String name){
        try{
            getEquipmentFromName(name);
            return true;
        } catch (IndexOutOfBoundsException ex){
            return false;
        }
    }

    public Equipment getEquipmentFromName(String name){
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("from Equipment where name = '" + name + "'");
        List<Equipment> list = query.list();
        session.getTransaction().commit();
        return list.get(0);
    }


    public void updateEquipment(Equipment equipment, String newName, String newPlaceName) throws InvalidFormatException, NameAlreadyInUseException {
        if(!equipment.getName().equals(newName)) {
            validateEquipmentsName(newName);
            equipment.setName(newName);
        }

        if(equipment.getPlace().getName() != newPlaceName){
            PlaceController controller = new PlaceController();
            Place place = controller.getPlaceFromName(newPlaceName);
            equipment.setPlace(place);
        }

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        session.update(equipment);
        session.getTransaction().commit();

    }

    private void validateEquipmentsName(String name) throws NameAlreadyInUseException, InvalidFormatException {
        if(isEquipmentNameAlreadyInUse(name))
            throw new NameAlreadyInUseException();

        if(!name.matches("^[A-Za-z0-9 ]{3,32}$"))
            throw new InvalidFormatException("Name in not valid.");
    }

    public void deleteEquipment(Equipment equipment){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(equipment);
        session.getTransaction().commit();
    }

}
