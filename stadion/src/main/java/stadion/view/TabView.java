package stadion.view;

import javax.swing.*;

/**
 * Created by grzegorz on 22.04.17.
 */
public interface TabView {
    JPanel getView();
    String getName();
}
