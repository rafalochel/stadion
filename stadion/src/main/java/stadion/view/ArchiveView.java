package stadion.view;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import stadion.controller.*;
import stadion.exception.UserDoesNotExistsException;
import stadion.model.Booking;
import stadion.model.Equipment;
import stadion.model.Place;
import stadion.model.User;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.print.Book;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by grzegorz on 21.04.17.
 */
public class ArchiveView implements TabView {
    private JPanel view;
    private JButton button_left;
    private JButton button_right;
    private JTable bookingsTable;
    private JButton setStatusButton;
    private JLabel data_label;
    private JButton showButton;
    private String name;
    private Date begin;
    private Date end;
    private final long week2ms = 604800000;
    private final long day2ms = 86400000;
    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private final static Date today = new Date();
    private List<Object[]> bookingList;

    public ArchiveView(String name) {
        this.name = name;
        end = new Date();
        begin = new Date(end.getTime() - week2ms + day2ms);
        setText(dateFormat.format(begin.getTime()), dateFormat.format(end.getTime()));

        bookingsTable.setRowSelectionAllowed(true);
        bookingsTable.setColumnSelectionAllowed(false);
        bookingsTable.getTableHeader().setReorderingAllowed(false);
        bookingsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        bookingsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if (bookingsTable.getSelectedRow() != -1 && bookingList.size() > 0) {
                    Booking booking = (Booking) bookingList.get(bookingsTable.getSelectedRow())[0];
                    if (booking.getBeginDate().before(today) && booking.getBeginTime().before(today)) {
                        if (booking.getStatus() == 0) {
                            setStatusButton.setEnabled(true);
                        } else {
                            setStatusButton.setEnabled(false);
                        }
                    } else {
                        setStatusButton.setEnabled(false);
                    }
                }
            }
        });

        button_left.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                begin.setTime(begin.getTime() - week2ms);
                end.setTime(end.getTime() - week2ms);
                setText(dateFormat.format(begin.getTime()), dateFormat.format(end.getTime()));


            }
        });

        button_right.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                button_left.setEnabled(true);
                begin.setTime(begin.getTime() + week2ms);
                end.setTime(end.getTime() + week2ms);
                setText(dateFormat.format(begin.getTime()), dateFormat.format(end.getTime()));

            }
        });

        showButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bookingList = ArchiveController.fillTable(bookingsTable, begin, end, true);
            }
        });

        setStatusButton.addActionListener(e -> {
            int row = bookingsTable.getSelectedRow();
            if (row == -1) return;
            String[] options = {"Collected", "Uncollected"};
            String choosen = (String) JOptionPane.showInputDialog(view,
                    "Choose status.",
                    "Status",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    options,
                    options[0]);
            if (choosen != null) {
                BookingController controller = new BookingController();
                int bookingId = ((Booking) bookingList.get(row)[0]).getId();
                if (choosen.equals("Collected")) {
                    controller.setBookingCollected(bookingId);
                } else if (choosen.equals("Uncollected")) {
                    controller.setBookingUncollected(bookingId);
                }
                bookingList = ArchiveController.fillTable(bookingsTable, begin, end, true);
            }
        });
    }

    public JPanel getView() {
        return view;
    }

    public String getName() {
        return name;
    }

    private void setText(String begin, String end) {
        data_label.setText(begin + " - " + end);
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        view = new JPanel();
        view.setLayout(new GridLayoutManager(4, 7, new Insets(0, 0, 0, 0), -1, -1));
        data_label = new JLabel();
        data_label.setText("01.01.1970-08.01.1970");
        view.add(data_label, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        view.add(spacer1, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        view.add(spacer2, new GridConstraints(0, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        button_left = new JButton();
        button_left.setText("<");
        view.add(button_left, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        button_right = new JButton();
        button_right.setText(">");
        view.add(button_right, new GridConstraints(0, 5, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        view.add(spacer3, new GridConstraints(0, 6, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer4 = new Spacer();
        view.add(spacer4, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        view.add(scrollPane1, new GridConstraints(2, 0, 1, 7, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        bookingsTable = new JTable();
        scrollPane1.setViewportView(bookingsTable);
        showButton = new JButton();
        showButton.setText("Show");
        view.add(showButton, new GridConstraints(1, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        setStatusButton = new JButton();
        setStatusButton.setEnabled(false);
        setStatusButton.setText("Set Status");
        view.add(setStatusButton, new GridConstraints(3, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return view;
    }
}
