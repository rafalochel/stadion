package stadion.view;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import stadion.controller.BookingController;
import stadion.controller.MainController;
import stadion.model.Booking;

import java.util.List;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rafal on 22.04.17.
 */
public class BookingView implements View {
    private JPanel bookingPanel;
    private Date begin;
    private Date end;
    private JButton button_left;
    private JButton button_right;
    private JPanel selection;
    private JLabel date_label;
    private JLabel[] days = new JLabel[7];
    JButton[][] buttons;
    private final long week2ms = 604800000;
    private final long day2ms = 86400000;
    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private final static Date today = new Date();
    private final int id;
    private final String name;
    String[] hours;
    public Timer timer;

    public BookingView(int ID, String pName) {
        id = ID;
        name = pName;
        for (int i = 0; i < 7; i++) {
            days[i] = new JLabel("Wartosc " + i);
        }

        begin = new Date();
        end = new Date(begin.getTime() + week2ms - day2ms);
        setText(dateFormat.format(begin.getTime()), dateFormat.format(end.getTime()));
        if (MainController.loggedUser.getRole() > 1) {
            button_left.setEnabled(false);
        }

        button_left.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                begin.setTime(begin.getTime() - week2ms);
                end.setTime(end.getTime() - week2ms);
                setText(dateFormat.format(begin.getTime()), dateFormat.format(end.getTime()));
                if (new Date(begin.getTime() - week2ms).before(today) && MainController.loggedUser.getRole() > 1) {
                    button_left.setEnabled(false);
                }
                tableLabels(begin);
            }
        });
        button_right.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                button_left.setEnabled(true);
                begin.setTime(begin.getTime() + week2ms);
                end.setTime(end.getTime() + week2ms);
                setText(dateFormat.format(begin.getTime()), dateFormat.format(end.getTime()));
                tableLabels(begin);
            }
        });
        for (int i = 0; i < 7; i++) {
            GridBagConstraints c = new GridBagConstraints();
            c.fill = GridBagConstraints.HORIZONTAL;
            c.gridx = i;
            c.gridy = 0;
            days[i].setHorizontalAlignment(JLabel.CENTER);
            selection.add(days[i], c);
        }
        tableLabels(begin);
        reserveButtons();
        ActionListener taskPerformer = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                refreshButtons();
            }
        };
        timer = new Timer(1000 / 24, taskPerformer);
        timer.start();
    }

    private void setText(String begin, String end) {
        date_label.setText(begin + " - " + end);
    }

    private void tableLabels(Date b) {
        for (int i = 0; i < 7; i++) {
            days[i].setText(dateFormat.format(begin.getTime() + i * day2ms));
        }
    }

    private void reserveButtons() {
        hours = new String[]{"08:00-09:30", "09:30-11:00", "11:00-12:30", "12:30-14:00", "14:00-15:30", "15:30-17:00", "17:00-18:30", "18:30-20:00", "20:00-21:30", "21:30-23:00"};
        buttons = new JButton[7][10];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 10; j++) {
                GridBagConstraints c = new GridBagConstraints();
                c.fill = GridBagConstraints.HORIZONTAL;
                c.gridx = i;
                c.gridy = j + 1;
                buttons[i][j] = new JButton();
                buttons[i][j].setText(hours[j]);
                String day = dateFormat.format(begin.getTime() + i * day2ms);
                String hour = hours[j].split("-")[0];
                JButton temp = buttons[i][j];
                buttons[i][j].addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent actionEvent) {
                        temp.setEnabled(false);
                        JFrame x = new JFrame("Stadion -  Equipment Selection");
                        ConfirmBookingView.frames.add(x);
                        x.setContentPane(new ConfirmBookingView(name, day, hour, id).getMainPanel());
                        x.setMinimumSize(new Dimension(440, 360));
                        x.setVisible(true);
                    }
                });
                selection.add(buttons[i][j], c);
            }
        }
    }

    private void refreshButtons() {
        BookingController bc = new BookingController();
        List<Booking> list = bc.getAllBookingsFromPlace(id);
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 10; j++) {
                String day = dateFormat.format(begin.getTime() + i * day2ms);
                String hour = hours[j].split("-")[0];
                if (bc.isReserved(id, day, hour, list)) {
                    buttons[i][j].setEnabled(false);
                } else {
                    buttons[i][j].setEnabled(true);

                }
            }
        }
    }

    public JPanel getMainPanel() {
        return bookingPanel;
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        bookingPanel = new JPanel();
        bookingPanel.setLayout(new GridLayoutManager(2, 7, new Insets(0, 0, 0, 0), -1, -1));
        button_left = new JButton();
        button_left.setText("<");
        bookingPanel.add(button_left, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        date_label = new JLabel();
        date_label.setText("01.01.1970-08.01.1970");
        bookingPanel.add(date_label, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        button_right = new JButton();
        button_right.setText(">");
        bookingPanel.add(button_right, new GridConstraints(0, 5, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        bookingPanel.add(spacer1, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        bookingPanel.add(spacer2, new GridConstraints(0, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        selection = new JPanel();
        selection.setLayout(new GridBagLayout());
        bookingPanel.add(selection, new GridConstraints(1, 0, 1, 7, GridConstraints.ANCHOR_NORTH, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        bookingPanel.add(spacer3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, new Dimension(10, -1), new Dimension(10, -1), new Dimension(10, -1), 0, false));
        final Spacer spacer4 = new Spacer();
        bookingPanel.add(spacer4, new GridConstraints(0, 6, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, new Dimension(10, -1), new Dimension(10, -1), new Dimension(10, -1), 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return bookingPanel;
    }
}
