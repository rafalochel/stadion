package stadion.view;

import javax.swing.*;

/**
 * Created by wiewiogr on 04.04.17.
 */
public interface View {
    JPanel getMainPanel();
}
