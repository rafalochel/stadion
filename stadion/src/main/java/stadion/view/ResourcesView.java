package stadion.view;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import stadion.controller.EquipmentController;
import stadion.controller.PlaceController;
import stadion.model.Equipment;
import stadion.model.Place;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.List;
import java.util.Set;

/**
 * Created by grzegorz on 21.04.17.
 */
public class ResourcesView implements TabView {
    private JPanel view;
    private JTable placesTable;
    private JTable equipmentsTable;
    private JButton addPlaceButton;
    private JButton editPlaceButton;
    private JButton addEquipmentButton;
    private JButton editEquipmentButton;
    private String name;
    private List<Place> places;

    ResourcesView(String name) {
        this.name = name;

        placesTable.setRowSelectionAllowed(true);
        placesTable.setColumnSelectionAllowed(false);
        placesTable.getTableHeader().setReorderingAllowed(false);
        placesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        placesTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                updateEquipmentTable();
            }
        });

        PlaceController controller = new PlaceController();
        places = controller.getAllPlaces();

        view.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                super.componentShown(e);
                fillPlacesTable();
            }
        });


        addPlaceButton.addActionListener((ActionEvent e) -> {
            PlaceAddView placeAddView = new PlaceAddView(this);
        });

        addEquipmentButton.addActionListener((ActionEvent e) -> {
            EquipmentAddView equipmentAddView = new EquipmentAddView(places, this);
        });

        editEquipmentButton.addActionListener(e -> {
            try {
                Place place = places.get(placesTable.getSelectedRow());
                String equipmentsName = (String) equipmentsTable.getValueAt(equipmentsTable.getSelectedRow(), 0);
                EquipmentController equipmentController = new EquipmentController();
                Equipment equipment = equipmentController.getEquipmentFromName(equipmentsName);
                new EquipmentEditView(equipment, places, this);
            } catch (Throwable ex) {
                JOptionPane.showMessageDialog(null, "No equipment selected.", "Stadion", JOptionPane.INFORMATION_MESSAGE);
            }
        });

        editPlaceButton.addActionListener(e -> {
            try {
                Place place = places.get(placesTable.getSelectedRow());
                new PlaceEditView(place, this);
            } catch (Throwable ex) {
                JOptionPane.showMessageDialog(null, "No place selected.", "Stadion", JOptionPane.INFORMATION_MESSAGE);
            }

        });
    }

    public JPanel getView() {
        return view;
    }

    public String getName() {
        return name;
    }

    private void fillPlacesTable() {
        placesTable.removeAll();
        String[] columns = {"Name", "Capacity"};
        Object[][] rowData = new Object[places.size()][columns.length];
        int i = 0;
        for (Place place : places) {
            rowData[i][0] = place.getName();
            rowData[i][1] = place.getCapacity();
            i++;
        }

        placesTable.clearSelection();
        placesTable.setModel(new DefaultTableModel(rowData, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

    }

    private void updateEquipmentTable() {
        int row = placesTable.getSelectedRow();
        if (row != -1) {
            Place place = places.get(placesTable.getSelectedRow());
            fillEquipmentTableFromPlace(place);
        }
    }

    private void fillEquipmentTableFromPlace(Place place) {
        String[] columns = {"Name"};
        Set<Equipment> equipments = place.getEquipments();
        Object[][] rowData = new Object[equipments.size()][columns.length];
        int i = 0;
        for (Equipment equipment : equipments) {
            rowData[i][0] = equipment.getName();
            i++;
        }
        equipmentsTable.setRowSelectionAllowed(true);
        equipmentsTable.setColumnSelectionAllowed(false);

        equipmentsTable.getModel();
        equipmentsTable.setModel(new DefaultTableModel(rowData, columns) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        });

    }

    public void refresh() {
        PlaceController controller = new PlaceController();
        places = controller.getAllPlaces();
        fillPlacesTable();
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        view = new JPanel();
        view.setLayout(new GridLayoutManager(3, 4, new Insets(0, 0, 0, 0), -1, -1));
        final JScrollPane scrollPane1 = new JScrollPane();
        view.add(scrollPane1, new GridConstraints(0, 2, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        equipmentsTable = new JTable();
        scrollPane1.setViewportView(equipmentsTable);
        final JScrollPane scrollPane2 = new JScrollPane();
        view.add(scrollPane2, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        placesTable = new JTable();
        scrollPane2.setViewportView(placesTable);
        addPlaceButton = new JButton();
        addPlaceButton.setText("Add Place");
        view.add(addPlaceButton, new GridConstraints(2, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        editEquipmentButton = new JButton();
        editEquipmentButton.setText("Edit Equipment");
        view.add(editEquipmentButton, new GridConstraints(2, 2, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        editPlaceButton = new JButton();
        editPlaceButton.setHorizontalTextPosition(11);
        editPlaceButton.setText("Edit Place ");
        view.add(editPlaceButton, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        addEquipmentButton = new JButton();
        addEquipmentButton.setEnabled(true);
        addEquipmentButton.setFont(new Font(addEquipmentButton.getFont().getName(), addEquipmentButton.getFont().getStyle(), addEquipmentButton.getFont().getSize()));
        addEquipmentButton.setHideActionText(true);
        addEquipmentButton.setHorizontalTextPosition(11);
        addEquipmentButton.setText("Add Equipment");
        view.add(addEquipmentButton, new GridConstraints(1, 2, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return view;
    }
}
