package stadion;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class DisplayImage implements Runnable{
    private BufferedImage img;
    private BufferedImage base;
    private JFrame frame;
    private ImageIcon icon;
    AffineTransformOp op;

    public DisplayImage(String title){
        InputStream x = getClass().getResourceAsStream("/loading.png");
        try {
            base = ImageIO.read(x);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        img = base;
        frame=new JFrame(title);
        frame.setUndecorated(true);
        frame.setSize(300,300);
        frame.setResizable(false);
        frame.setLayout(new GridLayout());
        frame.setBackground(new Color(255,255,255,0));
        frame.add(new JComponent() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2d = (Graphics2D) g;
                g2d.setColor(new Color(255,255,255,0));
                g2d.fillRect(0,0,256,256);
                g2d.drawImage(img,null, null);
            }
        });
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void run() {
        try {
            int x=0;
            while (true) {
                Thread.sleep(1);
                AffineTransform at = new AffineTransform();
                at.translate(128, 128);
                at.rotate(Math.PI/5120*x);
                at.translate(-128,-128);
                op = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
                img = op.filter(base,null);
                frame.repaint();
                x++;
                if(x>=5119)
                    x=0;
            }
        }
        catch (InterruptedException ex)
        {
        }
    }

    public void destroy()
    {
        frame.setVisible(false);
    }
}