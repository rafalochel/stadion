package stadion.exception;

/**
 * Created by wiewiogr on 13.04.17.
 */
public class InvalidFormatException extends Exception{
    String errors;
    public InvalidFormatException(String errors){
        this.errors = errors;
    }

    public String toString(){
        return errors;
    }
}
