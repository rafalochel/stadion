package stadion.model;

import java.util.Set;

/**
 * Created by wiewiogr on 04.04.17.
 */
public class User {
    public int id;
    private String name;
    private String surname;
    private String username;
    private String email;
    private String password;
    private boolean isActivated;
    private int role;
    private int reputation;
    private Set<Booking> bookings;

    public User(String name, String surname, String username, String email, String password, int role) {
        this.name = name;
        this.role = role;
        this.surname = surname;
        this.username = username;
        this.email = email;
        this.password = password;
        this.isActivated = false;
    }

    public User() {
        this.name = "name";
        this.role = -1;
        this.surname = "surname";
        this.username = "username";
        this.email = "email";
        this.password = "password";
        this.isActivated = false;
        this.reputation = 0;
    }

    //Have to be there
    private void setId(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getisActivated() {
        return isActivated;
    }

    public void setisActivated(boolean activated) {
        isActivated = activated;
    }

    public Set<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(Set<Booking> bookings) {
        this.bookings = bookings;
    }
    public boolean isActivated() {
        return role >=0;
    }

    public String getRoleName() {
        switch(role) {
            case 0: return "Administrator";
            case 1: return "Kierownik";
            case 2: return "Pracowik";
            case 3: return "Klient";
            case -1: return "Wymaga aktywacji";
            default: return "NIEZNANY";
        }
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }
}
