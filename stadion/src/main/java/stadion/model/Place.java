package stadion.model;

import java.util.Set;

/**
 * Created by rafal on 07.04.17.
 */
public class Place {
    private int id;
    private String name;
    private int capacity;
    private Set<Equipment> equipments;

    public Place(String name, int capacity) {
        this.name = name;
        this.capacity = capacity;
    }

    public Place() {
        this.id=-1;
        this.name="";
        this.capacity=0;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(Set<Equipment> equipments) {
        this.equipments = equipments;
    }
}