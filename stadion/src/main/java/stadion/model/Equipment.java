package stadion.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by wiewiogr on 22.04.17.
 */
public class Equipment implements Comparable<Equipment>{
    private int id;
    private String name;
    private Set<Booking> bookings = new HashSet<>();
    private Place place;

    public Equipment(){
        id = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Booking> getBookings() {
        return bookings;
    }

    public void setBookings(Set<Booking> bookings) {
        this.bookings = bookings;
    }

    @Override
    public int compareTo(Equipment o) {
        if(getId()<o.getId())
        {
            return -1;
        }
        else if(getId()>o.getId())
        {
            return 1;
        }
        else
            return 0;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }
}
